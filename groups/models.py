from django.db import models

# Create your models here.
class Groups(models.Model):
    name = models.CharField(max_length=80, null=False)
    count = models.IntegerField(null=True)
