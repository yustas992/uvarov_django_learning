from django.db import models
import datetime

# Create your models here.



class Teachers(models.Model):
    first_name = models.CharField(max_length=80, null=False)
    last_name = models.CharField(max_length=80, null=False)
    email = models.EmailField(max_length=120, null=True)
    entry_date = models.DateTimeField(null=True, default=datetime.date.today)
    leave_date = models.DateTimeField(null=True, default=datetime.date.today)

